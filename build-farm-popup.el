;;; build-farm-popup.el --- Magit-like popup interface  -*- lexical-binding: t -*-

;; Copyright © 2018 Alex Kost <alezost@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides popup interface (using `transient' library) for
;; build farm commands.

;;; Code:

(require 'cl-lib)
(require 'transient)
(require 'build-farm-url)
(require 'build-farm-build)
(require 'build-farm-evaluation)

(defgroup build-farm-popup nil
  "Variables for popup interface for build farm commands."
  :group 'build-farm)

;;;###autoload (autoload 'build-farm-popup "build-farm-popup" nil t)
(transient-define-prefix build-farm-popup ()
  "Show popup buffer for build farm commands."
  ["Variables"
   ("u" build-farm-set-url-from-popup)]
  ["Commands"
   ("p" "projects" build-farm-projects)
   ("j" "jobsets"  build-farm-jobsets)
   ("b" "builds"   build-farm-build-popup)
   ("e" "evaluations" build-farm-evaluation-popup
    ;; See https://github.com/NixOS/hydra/issues/582.
    :if-not (lambda () (eq (build-farm-url-type) 'hydra)))])

;;;###autoload
(defalias 'build-farm #'build-farm-popup
  "Popup interface for the available build farm commands.")

(transient-define-infix build-farm-set-url-from-popup ()
  "Set `build-farm-url' from a popup buffer."
  :class 'transient-lisp-variable
  :variable 'build-farm-url
  :reader (lambda (_prompt _default _history)
            (build-farm-read-url)))


;;; Builds

(transient-define-prefix build-farm-build-popup ()
  "Show popup buffer for builds."
  ["Variable for latest and queued builds"
   ("n" build-farm-set-number-of-builds-from-popup)]
  ["Options for latest builds"
   ("-p" "project" "project=" build-farm-popup-read-project)
   ("-J" "jobset"  "jobset=" build-farm-popup-read-jobset)
   ("-j" "job"     "job=")
   ("-s" "system"  "system=" build-farm-read-system)]
  ["Actions"
   ("l" "latest" build-farm-popup-latest-builds)
   ("q" "queued" build-farm-popup-queued-builds)
   ("i" "build by ID" build-farm-build)])

(transient-define-infix build-farm-set-number-of-builds-from-popup ()
  "Set `build-farm-number-of-builds' from a popup buffer."
  :class 'transient-lisp-variable
  :variable 'build-farm-number-of-builds
  :reader (lambda (_prompt _default _history)
            (build-farm-build-read-number)))

(defun build-farm-popup-read-project (&optional prompt initial-input _history)
  "Read project from minibuffer.
See `completing-read' for PROMPT and INITIAL-INPUT."
  (build-farm-read-project :prompt prompt
                           :initial-input initial-input))

(defun build-farm-popup-read-jobset (&optional prompt initial-input _history)
  "Read jobset for the current project from minibuffer.
See `completing-read' for PROMPT and INITIAL-INPUT."
  (build-farm-read-jobset
   :prompt prompt
   :initial-input initial-input
   :project (plist-get (build-farm-popup-parse-build-args
                        (transient-get-value))
                       :project)))

(defun build-farm-popup-parse-build-args (args)
  "Convert popup ARGS to a form suitable for `build-farm-latest-builds'."
  (cl-mapcan (lambda (string)
               (cl-multiple-value-bind (key value)
                   (split-string string "=")
                 (list (intern (concat ":" key))
                       value)))
             args))

(defun build-farm-popup-latest-builds (&rest args)
  "Display `build-farm-number-of-builds' of latest builds.
ARGS are read from the current popup buffer."
  (interactive (list (transient-args 'build-farm-build-popup)))
  (apply #'build-farm-latest-builds
         (apply #'build-farm-build-latest-prompt-args
                (build-farm-popup-parse-build-args args))))

(defun build-farm-popup-queued-builds ()
  "Display `build-farm-number-of-builds' of queued builds."
  (interactive)
  (build-farm-queued-builds
   (or build-farm-number-of-builds
       (build-farm-build-read-number))))


;;; Evaluations

(transient-define-prefix build-farm-evaluation-popup ()
  "Show popup buffer for evaluations."
  ["Variables"
   ("n" build-farm-set-number-of-evaluations-from-popup)]
  ["Actions"
   ("l" "latest" build-farm-popup-latest-evaluations)])

(transient-define-infix build-farm-set-number-of-evaluations-from-popup ()
  "Set `build-farm-number-of-evaluations' from a popup buffer."
  :class 'transient-lisp-variable
  :variable 'build-farm-number-of-evaluations
  :reader (lambda (_prompt _default _history)
            (build-farm-evaluation-read-number)))

(defun build-farm-popup-latest-evaluations ()
  "Display `build-farm-number-of-evaluations' of latest evaluations."
  (interactive)
  (build-farm-latest-evaluations
   (or build-farm-number-of-evaluations
       (build-farm-evaluation-read-number))))

(provide 'build-farm-popup)

;;; build-farm-popup.el ends here
